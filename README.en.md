# L-ETC-Pytorch

## Description
Pytorch implementation of L-ETC: A Lightweight Model Based on Key Bytes Selection for Encrypted Traffic Classification, which was accepted by ICC'23 CQRM Symposium.

## Public Dataset
Our encrypted traffic classification tasks are designed based on three of the most widely used datasets, including ISCX-VPN, ISCX-TOR and USTC-TFC, which can be obtained from Baidu Cloud or downloaded from the dataset source address for ease of use. (链接: https://pan.baidu.com/s/1AyMzsJGfc_iIBjL8hokvqA 提取码: cgv1)

## Installation


```
pip install -r requirements.txt
```



## Citation



```
@INPROCEEDINGS{Cao2305:L,
AUTHOR="Jie Cao and Yuwei Xu and Qiao Xiang",
TITLE="{L-ETC:} A Lightweight Model Based on Key Bytes Selection for Encrypted
Traffic Classification",
BOOKTITLE="2023 IEEE International Conference on Communications (ICC): Communication
QoS, Reliability and Modeling Symposium (IEEE ICC'23 - CQRM Symposium)",
ADDRESS="Rome, Italy",
DAYS="27",
MONTH=may,
YEAR=2023}
```



