import numpy as np
from joblib import Parallel, delayed
from scapy.compat import raw
from scapy.layers.inet import IP, UDP,TCP
from scapy.layers.l2 import Ether
from scapy.packet import Padding
from scipy import sparse
import os
from utils_ustc import should_omit_packet, read_pcap, PREFIX_TO_MALBEGIN_ID, ID_TO_Malben
from tqdm import tqdm
import random


def remove_ether_header(packet):
    if Ether in packet:
        return packet[Ether].payload

    return packet

def port_mask(packet):
    if TCP in packet:
        # get layers after udp
        packet[TCP].sport = 0
        packet[TCP].dport = 0
    elif UDP in packet:
        packet[UDP].sport = 0
        packet[UDP].dport = 0
    return packet

def mask_ip(packet):
    if IP in packet:
        packet[IP].src = '0.0.0.0'
        packet[IP].dst = '0.0.0.0'

    return packet


def pad_udp(packet):
    if UDP in packet:
        # get layers after udp
        layer_after = packet[UDP].payload.copy()

        # build a padding layer
        pad = Padding()
        pad.load = '\x00' * 12

        layer_before = packet.copy()
        layer_before[UDP].remove_payload()
        packet = layer_before / pad / layer_after
        return packet
        
    return packet



def packet_to_sparse_array(packet, max_length=50):
    arr = np.frombuffer(raw(packet), dtype=np.uint8)[0: max_length] #/ 255
    if len(arr) < max_length:
        pad_width = max_length - len(arr)
        arr = np.pad(arr, pad_width=(0, pad_width), constant_values=0)
    return arr


def transform_packet(packet):
    if should_omit_packet(packet):
        return None
    packet = remove_ether_header(packet)
    packet = mask_ip(packet)
    packet = port_mask(packet)
    packet = pad_udp(packet)
    
    arr = packet_to_sparse_array(packet)
    if arr is not None:
        token = ""
        for i in arr:
            token = token + " " + str(i)
        return token.strip(" ")

def transform_pcap(path):
    f_mal = open("L-etc_ustc.txt",'a')
    c = 0
    prefix = path.split('/')[-1].split('.')[0].lower()
    label = PREFIX_TO_MALBEGIN_ID.get(prefix)
    
    for i, packet in enumerate(read_pcap(path)):
        token = transform_packet(packet)
        if label is not None and token is not None:
            f_mal.write(token+"\t"+str(label)+"\n")

def all_path(dirname):
    result = []
    for maindir, subdir, file_name_list in os.walk(dirname):
        for filename in file_name_list:
            apath = os.path.join(maindir, filename)
            result.append(apath)
    return result

if __name__ == '__main__':
    # source: PCAP file path
    source =  "/home/dl/Desktop/program/Traffic_class/Public_dataset/USTC-TFC2016/packet/" 
    root = os.listdir(source)
    for i in tqdm(root):
        path = source + i
        transform_pcap(path)


    