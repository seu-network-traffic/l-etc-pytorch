from pathlib import Path

from scapy.layers.dns import DNS
from scapy.layers.inet import TCP
from scapy.packet import Padding
from scapy.utils import PcapReader
import os 


PREFIX_TO_MALBEGIN_ID = {
    # Benign
    'worldofwarcraft': 0,
    'bittorrent': 1,
    'smb-2': 2,
    'smb-1': 2,
    'outlook': 3,
    'gmail': 4,
    'weibo-1': 5,
    'weibo-4': 5,
    'weibo-2': 5,
    'weibo-3': 5,
    'mysql': 6,
    'ftp': 7,
    'facetime': 8,
    'skype': 9,
    # Malware
    'cridex': 10,
    'nsis-ay': 11,
    'geodo': 12,
    'htbot': 13,
    'tinba': 14,
    'shifu': 15,
    'miuref': 16,
    'zeus': 17,
    'virut': 18,
    'neris': 19
    
}


ID_TO_Malben = {
    0: 'worldofwarcraft',
    1: 'bittorrent',
    2: "smb",
    3: "outlook",
    4: "gmail",
    5: "weibo",
    6: "mysql",
    7: "ftp",
    8: "facetime",
    9: "skype",
    10: 'cridex',
    11: 'nsis-ay',
    12: 'geodo',
    13: 'htbot',
    14: 'tinba',
    15: 'shifu',
    16: 'miuref',
    17: 'zeus',
    18: 'virut',
    19: 'neris'
}


def read_pcap(path: Path):
    packets = PcapReader(str(path))

    return packets


def should_omit_packet(packet):
    # SYN, ACK or FIN flags set to 1 and no payload
    if TCP in packet and (packet.flags & 0x13):
        # not payload or contains only padding
        layers = packet[TCP].payload.layers()
        if not layers or (Padding in layers and len(layers) == 1):
            return True

    # DNS segment
    if DNS in packet:
        return True

    return False
